const express = require("express");
const app = express();
const random = require("random-name");
const uuid = require("uuid/v1");

app.use(express.json());

let users = {};


/**
 * Add header to enable CORS requests for local development.
 * Without this, you cannot access this API from a client running on localhost
 */
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
  
/** 
 * GET All Users
 */
app.get("/users", (req, res) => {

    let MAX = 100;

    if (Object.keys(users).length < MAX) {
    
        for (let i = 0; i < MAX; i++) {

            let id = uuid();

            users[id] = {
                id,
                name: {
                    first: random.first(),
                    middle: random.middle(),
                    last: random.last()
                }
            };
        }

    }    

    res.send(Array.from(Object.keys(users), id => users[id]));

});

/**
 * GET User by ID
 */
app.get("/users/:id", (req, res) => {

    let {id} = req.params;

    if(users[id]){
        res.json(users[id]);
    } else {
        res.sendStatus(404);
    }

});

/**
 * POST New User
 */
app.post("/users", (req, res) => {

    let validUser = validateUser(req.body);

    if(validUser) {
        validUser.id = uuid();
        users[validUser.id] = validUser;
        res.status(201).json(validUser);
    } else {
        res.sendStatus(500);
    }

});


validateUser = (user) => {
    
    let result;

    if(user.name) {

        let {first, middle, last} = user.name;

        if (first && last) {
            result = {
                name: {
                    first,
                    middle,
                    last
                }
            };
        } 

    }

    return result;

}

/**
 * Start Server
 */
app.listen(3001, () => {
    console.log("Listening on port 3001!");
})